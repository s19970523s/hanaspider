# 搭配hana project使用的網路爬蟲
## 說明
建立flask伺服器後，透過Request將台灣各大新聞網頭條新聞的標題、第一段內容傳回給filter，由filter將結果傳給使用者。

---

## 使用到的lib
* flask
* flask_restful (預計使用，主要用於swagger與RESTful)
> 參考網址: [使用 Serverless/Flask/Swagger 在 AWS 上搭建打造 Open API](https://medium.com/@nijia.lin/%E4%BD%BF%E7%94%A8-serverless-flask-swagger-%E5%9C%A8-aws-%E4%B8%8A%E6%90%AD%E5%BB%BA%E6%89%93%E9%80%A0-open-api-fb2baa0c0319)
* Requests (用於爬蟲)